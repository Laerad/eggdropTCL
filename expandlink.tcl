# This software is distibuted under the MIT license.
# The detailed license can be found at the bottom of this file.

## Expand links
#
# This script looks up URLs mentioned in the chat, and fetches some basic info.
# Behaviour:
# - parses every URL on a line of chat
# - Checks for redirects (with protection against loops)
# - Checks meta data (video & image)
# - Gets the <title> of text content
# - Uses cookies to break out of certain redirects
# The info displayed on the channel contains the final redirected link, and the title, or content type.


## Config:
# Maximum redirects to follow before giving up?
set expandlink(maxRedirects) 5   ;# <integer>
# Report link in reply?
# 0 = No link reporting
# 1 = Report only redirected links
# 2 = Report all links
set expandlink(linkreporting) 1

# Debug feedback
#  Display headers of content on the patyline?
set expandlink(debug,head) 1     ;# <boolean>


####
package require http
package require tls
bind pubm - "% *" pubm:expandlink:parseline

proc pubm:expandlink:parseline {nick uhost hand chan arg} {
  foreach l [expandlink:getlinks $arg] {
    # look up the URL and fetch the title
    set ::expandlink(redir) 1
    set data [expandlink:geturl $l]
    set title [lindex $data 1]
    unset ::expandlink(redir)
    if {[set link [lindex $data 0]]==0} {return}
    unset ::expandlink(Cookie)
    if {$::expandlink(linkreporting)==1&&![string equal -nocase $l $link]} {
      putquick "privmsg $chan :\002<$nick>\002: $title -=- Redirects to: $link"
    } elseif {$::expandlink(linkreporting)==2} {
      putquick "privmsg $chan :\002<$nick>\002: $title -=- $link"
    } {
      putquick "privmsg $chan :\002<$nick>\002: $title"
    }

  }
}

proc expandlink:geturl {link {h 1}} {
  if {[string range $link 0 5]=="https:"} {
    ::http::register https 443 [list ::tls::socket -tls1 1]
  } elseif {[string range $link 0 4]!="http:"} {
    return 0
  }
  ::http::config -useragent "Mozilla/5.0 (Macintosh; Insftel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0"
  # Infinite redirect loop combo breaker
  if {$::expandlink(redir)>=$::expandlink(maxRedirects)} {
    return [list $link "Error: Maximum redirects exceeed!"]
  }
  if {![info exists ::expandlink(Cookie)]} {set ::expandlink(Cookie) [list]}
  #  Get HEAD or GET
  if {[catch {set token [::http::geturl $link -validate $h -timeout 5000 -headers [list Cookie [join $::expandlink(Cookie) {;}]]]} error] == 1} {
    set r $error
  } elseif {[::http::ncode $token] == "404"} {
    set r "Error: 404"
  } elseif {[::http::status $token] == "ok"} {
    if {$h} {
      # Get the metadata - fix the case for httpds not following RFC
      # I'm looking at you Twitter.
      foreach {m con} [::http::meta $token] { set meta([string tolower $m]) $con }
      if {$::expandlink(debug,head)} {
        putlog "Meta for: $link:"
        foreach {i j} [array get meta] {putlog "\002*\002 $i -> $j"}
      }
      # Get a Cookie.
      if {[info exists meta(set-cookie)]} {
        lappend ::expandlink(Cookie) [lindex [split $meta(set-cookie) ";"] 0]
      }
      # Location is a different link than the one we're getting, get the headers again
      if {[info exists meta(location)] && ![string equal -nocase $meta(location) $link] && [llength [expandlink:getlinks $meta(location)]]} {
        incr ::expandlink(redir)
        set data [expandlink:geturl $meta(location)]
        set link [lindex $data 0]
        set r [lindex $data 1]
      } elseif {[info exists meta(content-type)]} {
        if {[string match -nocase *text/html* $meta(content-type)]} {
          set r [lindex [expandlink:geturl $link 0] 1]
        } {
          set r "$meta(content-type) [expandlink:getsize [array get meta]]"
        }
      } {
        set r "Error: Page has no Content-Type [expandlink:getsize [array get meta]]"
      }
    } {
      set data [::http::data $token]
      set mdata [string tolower $data]
      # find <title *>
      set from [string first ">" $mdata [string first "<title" $mdata 0]+6]
      set to [string first "</title" $mdata $from]
      set r [string map $::entitiesMap [string trim [string range $data $from+1 $to-1]]]
      if {$from == 6} {set r "No Title"}
    }
  } elseif {[::http::status $token] == "timeout"} {
    set r "Error: connection timeout [expandlink:getsize [array get meta]]"
  } elseif {[::http::status $token] == "error"} {
    set r "Error: [::http::error $token]"
  }
  if {[info exists token]} {::http::cleanup $token}
  return [list $link $r]
}

# Check if a string contains URLs and return them:
proc expandlink:getlinks {string} {
  set r [list]
  foreach {l c c c c c}  [regexp -all -inline {(?i)((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))} $string] { #" < because mcedit's formatting is stupid.
    lappend r $l
  }
  return $r
}

# Get the meta Content-Length tag
proc expandlink:getsize {metacontent} {
  array set meta $metacontent
  if {[info exists meta(content-length)]} {
    return [expandlink:makereadable $meta(content-length)]
  } {
    return "- \002Unknown size!\002"
  }
}

# Translate byte count to a human readable format:
proc expandlink:makereadable {bytes} {
  lappend t 0 B 1 KB 2 MB 3 GB 4 TB 5 PB 6 EB 7 YB
  foreach {e u} $t {
    if {[set tmp [expr {$bytes/(1024.0**$e)}]] && $tmp<1.0} {break}
    set size $tmp
    set unit $u
  }
  return "\002(\002[format "%.2f" $size] $unit\002)\002"
}

# take care of html entities
proc init:createmap {} {
  lappend m \r "" \n "" \r\n "" "&quot;" {"} "&gt;" ">" "&lt;" "<" "&nbsp;" " " "&amp;" "&" "&#8217" "'" ;#" < another formatting fix
  for {set i 0} {$i < 256} {incr i} {
    lappend m "&#$i;" [set s [format %c $i]]
    lappend m "&#0$i;" $s
  }
  # override chars
  lappend m ’ ' "&lquot;" ' "&lquot;" '
  set ::entitiesMap $m
}
init:createmap

putlog "\002\[Laerad\]\002 expandlink.tcl v0.31 -=- Loaded"
# Latest version: https://gitlab.com/Laerad/eggdropTCL
#
# Copyright (c) 2016 Katorone (https://gitlab.com/Laerad/eggdropTCL)
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Changelog
# 2016-02-05 - v0.1  - Creation
# 2016-02-06 - v0.15 - Added support for meta data and redirects
# 2016-02-06 - v0.2  - Nested procedure with protection against infinite redirects
# 2016-02-06 - v0.21 - Added image reporting support
# 2016-02-06 - v0.22 - Fixed a bug with redirect where it gave up too quickly
# 2016-02-06 - v0.23 - Added cookie support
# 2016-02-06 - v0.24 - Another small bugfix, added support for exotic but valid tags, and linebreaks
# 2016-02-07 - v0.25 - Changed how content-type is matched.  Only text/html; gets processed
# 2016-02-07 - v0.26 - Check the Location for a valid URL, to deal with broken webservers
# 2016-02-07 - v0.27 - Added reporting of content size for non text/html
#                      Not detecting size of html, because reporting this is useless thanks to images
#                      other inclusions like frames and dynamic content
# 2016-02-07 - v0.28 - Added support for html numerics with three digits where there should be two.
# 2016-02-08 - v0.29 - Added header support for servers not folliwng RFC (lowercase headers).
# 2016-02-08 - v0.3  - Rewrote the reporting to provide an option for when a link should be included.
# 2016-02-08 - v0.31 - Check that only the http and https protocols are used
